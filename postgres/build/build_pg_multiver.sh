#!/bin/bash

# buildPg.sh

installRoot=/home/scottm/software/pg
buildRoot=$installRoot/build
pgFilePrefix="postgresql-"
pgFilePostfix=".tar.bz2"

test -d $buildRoot
if [ "$?" -ne 0 ] 
then
    mkdir -p $buildRoot
fi

pushd $buildRoot

for version in 8.3.23 8.4.22 9.0.23 9.1.19 9.2.14 9.3.10 9.4.5
do
    vnodot=`echo $version | sed 's/\.//g' | sed 's/[a-zA-Z]*//g'`
    printf "Preparing for $version\n"
    dirname=$pgFilePrefix"$version"
    tarball=$dirname$pgFilePostfix
    
    # Don't re-download the tarball
    test -f $tarball
    
    if [ "$?" -ne 0 ] 
    then
        wget https://ftp.postgresql.org/pub/source/v$version/$tarball
    fi
    
    # Don't extract if the dir already exists
    test -d $dirname

    if [ "$?" -ne 0 ] 
    then
        tar -xvf $tarball
    fi
    
    pushd $dirname
   
    test -f "config.log"
    
    if [ "$?" -ne 0 ] 
    then
        make distclean
    fi
    
    ./configure --prefix=$installRoot/$version --with-openssl --with-ldap \
                                               --disable-rpath --with-perl \
                                               --with-python 

    if [ "$?" -ne 0 ] 
    then
        exit 1
    fi
    
    make -j6 && make install

    if [ "$?" -ne 0 ] 
    then
        exit 1
    fi
    
    while [ "$vnodot" -lt 1024 ]
    do
        vnodot=$(($vnodot * 10))
    done
    
    #Build an ENV file
    cat <<_EOF_ > $installRoot/$version/this.env
export PGHOME=$installRoot/$version
export PATH=\$PGHOME/bin:\$PATH
export PGDATA=$installRoot/$version/data
export PGPORT=$vnodot
export PGUSER=postgres
export LD_LIBRARY_PATH=\$PGHOME/lib
export PGDATABASE=postgres
_EOF_

    chmod 0755 $installRoot/$version/this.env
    
    source $installRoot/$version/this.env
    
    initdb -U postgres
    
    popd
    
done
    
